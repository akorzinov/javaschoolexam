package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static String evaluate(String statement) {
        if (statement == null ||
                statement.isEmpty() ||
                statement.contains("++") ||
                statement.contains("--") ||
                statement.contains("**") ||
                statement.contains("//") ||
                statement.contains("..") ||
                statement.contains(",") ||
                statement.contains("+-") ||
                statement.contains("-+") ||
                statement.contains("+*") ||
                statement.contains("*+") ||
                statement.contains("+/") ||
                statement.contains("/+") ||
                statement.contains("+.") ||
                statement.contains(".+") ||
                statement.contains("-*") ||
                statement.contains("*-") ||
                statement.contains("-/") ||
                statement.contains("/-") ||
                statement.contains("-.") ||
                statement.contains(".-") ||
                statement.contains("*/") ||
                statement.contains("/*") ||
                statement.contains("*.") ||
                statement.contains(".*") ||
                statement.contains("/.") ||
                statement.contains("./") ||
                statement.charAt(0) == '+' ||
                statement.charAt(0) == '*' ||
                statement.charAt(0) == '/' ||
                statement.startsWith("-("))
            return null;
        int countParentheses = 0;
        for (char i : statement.toCharArray()) {
            if (i == '(') countParentheses++;
            else if (i == ')') countParentheses--;
        }
        if (!(countParentheses == 0)) return null;
        statement = statement.replace(" ","");
        ArrayList<String> stackForNumbers = new ArrayList<>();
        ArrayList<Character> stackForOther = new ArrayList<>();
        int count = 0;
        int subcount = 0;
        while (count < statement.length()){
            if (Character.isDigit(statement.charAt(count)) || statement.charAt(count) == '.') { /*возможно здесь надо сделать проверку на отрицательность первого числа */
                if (count + 1 >= statement.length()) {
                    count++;
                    subcount++;
                    stackForNumbers.add(statement.substring(count - subcount, count));
                }
                else if (!Character.isDigit(statement.charAt(count + 1)) && !(statement.charAt(count + 1) == '.')){
                    stackForNumbers.add(statement.substring(count - subcount, count + 1));
                }
                count++;
                subcount++;
            }
            else {
                char temp = statement.charAt(count);
                if (stackForOther.isEmpty()) {
                    stackForOther.add(temp);
                    count++;
                    subcount = 0;
                }
                else if (priority(stackForOther.get(stackForOther.size()-1)) == 1 && priority(temp) == 2) {
                    stackForOther.add(temp);
                    count++;
                    subcount = 0;
                }
                else if (((priority(stackForOther.get(stackForOther.size()-1)) == 2 && priority(temp) == 1)) ||
                        ((priority(stackForOther.get(stackForOther.size()-1)) == 2 && priority(temp) == 2))  ||
                        ((priority(stackForOther.get(stackForOther.size()-1)) == 1 && priority(temp) == 1))) {
                    if (stackForOther.get(stackForOther.size() - 1) == '*') {
                        Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) * Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForOther.remove(stackForOther.size()-1);
                        stackForNumbers.add(intermediateResult.toString());
                        stackForOther.add(temp);
                        count++;
                        subcount = 0;
                    } else if (stackForOther.get(stackForOther.size() - 1) == '/') {
                        if (stackForNumbers.get(stackForNumbers.size() - 1).equals("0.0") || stackForNumbers.get(stackForNumbers.size() - 1).equals("0")) return null;
                        Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) / Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForOther.remove(stackForOther.size()-1);
                        stackForNumbers.add(intermediateResult.toString());
                        stackForOther.add(temp);
                        count++;
                        subcount = 0;
                    } else if (stackForOther.get(stackForOther.size() - 1) == '+') {
                        Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) + Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForOther.remove(stackForOther.size()-1);
                        stackForNumbers.add(intermediateResult.toString());
                        stackForOther.add(temp);
                        count++;
                        subcount = 0;
                    } else if (stackForOther.get(stackForOther.size() - 1) == '-') {
                        Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) - Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForNumbers.remove(stackForNumbers.size() - 1);
                        stackForOther.remove(stackForOther.size()-1);
                        stackForNumbers.add(intermediateResult.toString());
                        stackForOther.add(temp);
                        count++;
                        subcount = 0;
                    }
                } else if (temp == ')'){
                    while (!(stackForOther.get(stackForOther.size()-1) =='(')) {
                        if (stackForOther.get(stackForOther.size()-1) == '*') {
                            Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size()-2)) * Double.valueOf(stackForNumbers.get(stackForNumbers.size()-1));
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForOther.remove(stackForOther.size()-1);
                            stackForNumbers.add(intermediateResult.toString());
                        }
                        else if (stackForOther.get(stackForOther.size()-1) == '/') {
                            if (stackForNumbers.get(stackForNumbers.size() - 1).equals("0.0") || stackForNumbers.get(stackForNumbers.size() - 1).equals("0")) return null;
                            Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size()-2)) / Double.valueOf(stackForNumbers.get(stackForNumbers.size()-1));
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForOther.remove(stackForOther.size()-1);
                            stackForNumbers.add(intermediateResult.toString());
                        }
                        else if (stackForOther.get(stackForOther.size()-1) == '-') {
                            Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size()-2)) - Double.valueOf(stackForNumbers.get(stackForNumbers.size()-1));
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForOther.remove(stackForOther.size()-1);
                            stackForNumbers.add(intermediateResult.toString());
                        }
                        else if (stackForOther.get(stackForOther.size()-1) == '+') {
                            Double intermediateResult = Double.valueOf(stackForNumbers.get(stackForNumbers.size()-2)) + Double.valueOf(stackForNumbers.get(stackForNumbers.size()-1));
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForNumbers.remove(stackForNumbers.size() - 1);
                            stackForOther.remove(stackForOther.size()-1);
                            stackForNumbers.add(intermediateResult.toString());
                        }
                    }
                    stackForOther.remove(stackForOther.size()-1);
                    count++;
                    subcount = 0;
                }
                else {
                    stackForOther.add(temp);
                    count++;
                    subcount = 0;
                }
            }
        }
        if (!stackForOther.isEmpty()) {
            for (int i = 0; i < stackForNumbers.size(); i++) {
                if (stackForOther.get(stackForOther.size() - 1) == '*') {
                    Double result = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) * Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForOther.remove(stackForOther.size() - 1);
                    stackForNumbers.add(result.toString());
                } else if (stackForOther.get(stackForOther.size() - 1) == '/') {
                    if (stackForNumbers.get(stackForNumbers.size() - 1).equals("0.0") || stackForNumbers.get(stackForNumbers.size() - 1).equals("0"))
                        return null;
                    Double result = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) / Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForOther.remove(stackForOther.size() - 1);
                    stackForNumbers.add(result.toString());
                } else if (stackForOther.get(stackForOther.size() - 1) == '+') {
                    Double result = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) + Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForOther.remove(stackForOther.size() - 1);
                    stackForNumbers.add(result.toString());
                } else if (stackForOther.get(stackForOther.size() - 1) == '-') {
                    Double result = Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 2)) - Double.valueOf(stackForNumbers.get(stackForNumbers.size() - 1));
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForNumbers.remove(stackForNumbers.size() - 1);
                    stackForOther.remove(stackForOther.size() - 1);
                    stackForNumbers.add(result.toString());
                }
            }
        }
        if (Double.valueOf(stackForNumbers.get(0)) % 1 == 0) {
            Long roundResult = Math.round(Double.valueOf(stackForNumbers.get(0)));
            stackForNumbers.set(0,roundResult.toString());
            return stackForNumbers.get(0);
        }
        else return stackForNumbers.get(0);
    }

    public static int priority(char operand) {
        if (operand == '*' || operand == '/') return 2;
        else if (operand == '+' || operand == '-') return 1;
        else return 0;
    }
}