package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int height = validateSize(inputNumbers.size());
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        Collections.sort(inputNumbers);
        int index = 0;
        int [][] pyramid = new int[height][height * 2 - 1];
        for (int i = 0; i < height; i++) {
            int k = height - i - 1;
            for (int j = 0; j <= i; j++) {
                pyramid[i][k] = inputNumbers.get(index);
                index++;
                k+=2;
            }
        }
        return pyramid;
    }

    public static int validateSize(int size) {
        if (size == 0) throw new CannotBuildPyramidException();
        int count = 1;
        while (size > 0) {
            size = size - count;
            count++;
        }
        int height = count - 1;
        if (size == 0) return height;
        else throw new CannotBuildPyramidException();
    }
}
