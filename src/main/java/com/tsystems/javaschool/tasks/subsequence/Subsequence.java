package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) throw new IllegalArgumentException();
        if ((x.isEmpty() && y.isEmpty()) || (x.isEmpty() && !y.isEmpty())) return true;
        if (!x.isEmpty() && y.isEmpty()) return false;
        compare(x, y,0);
        if (x.equals(y)) return true;
        else return false;
    }

    public void compare(List x, List y, int count) {
        if (x.equals(y)) return;
        if (y.isEmpty()) return;
        if (x.size() == count) {
            y.remove(count);
            compare(x,y,count);
            return;
        }
        if (y.size() == count) return;
        if (x.get(count).equals(y.get(count))) {
            compare(x, y, count + 1);
        }
        else {
            y.remove(count);
            compare(x, y, count);
        }
    }
}
